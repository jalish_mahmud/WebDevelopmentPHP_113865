<?php
session_start();
use App\BITM\SEIP113865\Email\Email;
include_once '../../../../vendor/autoload.php';

$books = new Email();
$allBook = $books->bin();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
		<meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
        <title>CRUD | Jalish</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta name="description" content="3D Gallery with CSS3 and jQuery" />
        <meta name="keywords" content="3d, gallery, jquery, css3, auto, slideshow, navigate, mouse scroll, perspective" />
        <meta name="author" content="Codrops" />
        <link rel="shortcut icon" href="../favicon.ico"> 
        <link rel="stylesheet" type="text/css" href="../../../../css/font-awesome.min.css" />
        <link rel="stylesheet" type="text/css" href="../../../../css/demo.css" />
        <link rel="stylesheet" type="text/css" href="../../../../css/bootstrap.css" />
		<link rel="stylesheet" type="text/css" href="../../../../css/style.css" />
		<script type="text/javascript" src="../../../../js/modernizr.custom.53451.js"></script>
    </head>
    <body>
        <div class="container">
			<header>
				<h1>CRUD SYSTEM<span> BY  Jalish Mahmud</span></h1>
				<nav class="codrops-demos">
					<a class="current-demo" href="index.html">Home</a>
					<a href="">Birthday</a>
					<a href="">Summary</a>
					<a href="">Email</a>
					<a href="">Profile</a>
					<a href="">Gender</a>
					<a href="">Terms</a>
					<a href="">Hobby</a>
					<a href="">City</a>
					<a href="">Slider</a>
				</nav>
			</header>
        </div>
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="wrapper">
						<h2 >All Email List</h2>
						<div style="color:#70957C;">
							<?php
							if(isset($_SESSION['msg'])){
								echo $_SESSION['msg'];
								unset($_SESSION['msg']);
							}
							?>
						</div>
						<a class="btn btn-primary btn-xs page_link" href="show.php">Show All Email</a>
						<div class="table_wrap">
							<table class="table table-striped ">
								<tr class="warning">
									<td class="bold_item">SL</td>
									<td class="bold_item">ID</td>
									<td class="bold_item">EMAIL</td>
									<td class="bold_item">ACTIONS</td>
								</tr>
								<?php
								if(!empty($allBook)){
									$sl = 1;
									foreach($allBook as $book){
								?>
								<tr>
									<td><?php echo $sl++ ;?></td>
									<td><?php echo $book['id'] ;?></td>
									<td><?php echo $book['title'] ;?></td>
									<td>
										<a class="btn btn-default btn-xs" href="recover.php?id=<?php echo $book['id']?>">Recover</a>
										<a class="btn btn-default btn-xs" href="delete.php?id=<?php echo $book['id']?>">Delete</a>
									</td>
								</tr>
									<?php }} else{?>
									<tr>
										<td colspan="4"><?php echo "Opps! not no data found here" ;?></td>
									</tr>
								<?php }?>
							</table>

						</div>
					</div>
				</div>
			</div>
		</div>
    </body>
</html>