<?php
error_reporting ( E_ALL ^ E_DEPRECATED );

use App\BITM\SEIP113865\Profilepic\Profilepic;
include_once '../../../../vendor/autoload.php';

$ob = new Profilepic ();

$data = $ob->index ();
function debug($data = "") {
	echo '<pre>';
	print_r ( $data );
	echo '</pre>';
}




?>

<!DOCTYPE HTML>
<!--
	Visualize by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>Visualize by TEMPLATED</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body>

		<!-- Wrapper -->
			<div id="wrapper">

				
				<!-- Main -->
				
					<section id="main">
						<h2 align="center">All Profile Pictures</h2>
						
						<!-- Thumbnails -->
							<section >
							<table border="1">
							<tr> <td>Sl</td><td>Name</td><td>Profile Picture</td><td>Action</td></tr>
							<?php  $sl = 1; foreach ($data as $sdata) {  ?>
								<tr> 
									<td><?php echo $sl++;?></td>
									<td><?php echo $sdata['title'];?></td>
									<td><img src="img/<?php echo $sdata['profilepic']?>" alt="" height="200" width="300"/></td>
									<td>
									<a href="profilepic.php?id=<?php echo $sdata['id'];?>" > Make it profile photo </a>
									</td>
								</tr>
								<?php } ?>
								
							</table>	
							</section>
							<a href="index.php" style="text-align:center"> Go to Home </a> |
						<a href="viewprofilepic.php" style="text-align:center"> See Profile Picture </a>
					</section>



			</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.poptrox.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>