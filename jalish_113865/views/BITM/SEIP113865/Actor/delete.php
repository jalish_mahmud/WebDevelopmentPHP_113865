<?php
session_start();

error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../../../../vendor/autoload.php';

use App\BITM\SEIP113865\Actor\Actor;

$ob = new Actor();

$id = $_GET['id'];

$data = $ob->delete($id);