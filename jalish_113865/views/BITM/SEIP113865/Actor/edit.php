<?php 
session_start();

error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../../../../vendor/autoload.php';

use App\BITM\SEIP113865\Actor\Actor;

$ob = new Actor();

$id = $_GET['id'];

$data = $ob->single($id);

$string = $data['actor'];

$actors = explode(",", $string);



?>





<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>CRUD | Jalish</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="3D Gallery with CSS3 and jQuery" />
    <meta name="keywords" content="3d, gallery, jquery, css3, auto, slideshow, navigate, mouse scroll, perspective" />
    <meta name="author" content="Codrops" />
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" type="text/css" href="../../../../css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="../../../../css/demo.css" />
    <link rel="stylesheet" type="text/css" href="../../../../css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="../../../../css/style.css" />
    <script type="text/javascript" src="../../../../js/modernizr.custom.53451.js"></script>
</head>
<body>
<div class="container">
    <header>
        <h1>CRUD SYSTEM<span> BY  Jalish Mahmud</span></h1>
        <nav class="codrops-demos">
            <a class="current-demo" href="index.html">Home</a>
            <a href="">Birthday</a>
            <a href="">Summary</a>
            <a href="">Email</a>
            <a href="">Profile</a>
            <a href="">Gender</a>
            <a href="">Terms</a>
            <a href="">Hobby</a>
            <a href="">City</a>
            <a href="">Slider</a>
        </nav>
    </header>
</div>
<div class="container">
    <div class="row">

        <div class="col-sm-12">
            <div class="wrapper">
                <h2 >Add New Book</h2>

                <form class="form-inline" method="post" action="update.php">
                    <div class="form-group">
                        <label for="exampleInputName2">Your Name:</label>
                        <input type="text" name="title" value="<?php echo $data['title']; ?>" required="required" class="form-control" id="exampleInputName2">
                    </div><br/>
                    <label for="">Select Your Favorite Actor:</label><br/>
                    <input type="checkbox" name="actor[]" value="Deepika" <?php if(in_array('Deepika', $actors)){echo 'checked';}  ?>  />Deepika<br/>
                    <input type="checkbox" name="actor[]" value="khan" <?php if(in_array('khan', $actors)){echo 'checked';}  ?> />S R khan<br/>
                    <input type="checkbox" name="actor[]" value="Ranveer" <?php if(in_array('Ranveer', $actors)){echo 'checked';}  ?> />Ranveer<br/>
                    <input type="checkbox" name="actor[]" value="Neha" <?php if(in_array('Neha', $actors)){echo 'checked';}  ?> />Neha<br/>

                    <input type="hidden" name="id" value="<?php echo $id; ?>" />
                    <input type="submit" class="btn btn-default" value="UPDATE">
                </form>
                <a class="btn btn-primary btn-xs page_link" href="show.php">Show All Actors</a>
                <div style="color:#70957C;">
                    <?php
                    if(isset($_SESSION['msg'])){
                        echo $_SESSION['msg'];
                        unset($_SESSION['msg']);
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

