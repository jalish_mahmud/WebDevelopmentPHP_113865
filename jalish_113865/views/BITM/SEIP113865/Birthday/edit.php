<?php
session_start();
use App\BITM\SEIP113865\Birthday\Birthday;
include_once '../../../../vendor/autoload.php';

$books = new Birthday();
$id = $_GET['id'];
$singleBook = $books->single($id);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>CRUD | Jalish</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="3D Gallery with CSS3 and jQuery" />
    <meta name="keywords" content="3d, gallery, jquery, css3, auto, slideshow, navigate, mouse scroll, perspective" />
    <meta name="author" content="Codrops" />
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" type="text/css" href="../../../../css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="../../../../css/demo.css" />
    <link rel="stylesheet" type="text/css" href="../../../../css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="../../../../css/style.css" />
    <script type="text/javascript" src="../../../../js/modernizr.custom.53451.js"></script>
</head>
<body>
<div class="container">
    <header>
        <h1>CRUD SYSTEM<span> BY  Jalish Mahmud</span></h1>
        <nav class="codrops-demos">
            <a class="current-demo" href="index.html">Home</a>
            <a href="">Birthday</a>
            <a href="">Summary</a>
            <a href="">Email</a>
            <a href="">Profile</a>
            <a href="">Gender</a>
            <a href="">Terms</a>
            <a href="">Hobby</a>
            <a href="">City</a>
            <a href="">Slider</a>
        </nav>
    </header>
</div>
<div class="container">
    <div class="row">

        <div class="col-sm-12">
            <div class="wrapper">
                <h2 >Add New Birthday</h2>

                <form class="form-inline" method="post" action="update.php">
                    <div class="form-group">
                        <label for="exampleInputName2">Your Birthday:</label>
                        <input type="text" name="book" value="<?php echo $singleBook['title'];?>" required="required" class="form-control" id="exampleInputName2" placeholder="Book name">
                        <input type="hidden" name="id" value="<?php echo $id; ?>">
                    </div>
                    <input type="submit" class="btn btn-default" value="SAVE">
                </form>
                <a class="btn btn-primary btn-xs page_link" href="show.php">Show All Birthday</a>
                <div style="color:#d4b146;">
                    <?php
                    if(isset($_SESSION['msg'])){
                        echo $_SESSION['msg'];
                        unset($_SESSION['msg']);
                    }
                    ?></div>
            </div>
        </div>
    </div>
</div>
</body>
</html>