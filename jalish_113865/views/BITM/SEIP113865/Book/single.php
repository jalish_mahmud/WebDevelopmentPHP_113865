<?php
use App\BITM\SEIP113865\Book\Book;
include_once '../../../../vendor/autoload.php';

$books = new Book();
$id = $_GET['id'];
$singleBook = $books->single($id);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>CRUD | Jalish</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="3D Gallery with CSS3 and jQuery" />
    <meta name="keywords" content="3d, gallery, jquery, css3, auto, slideshow, navigate, mouse scroll, perspective" />
    <meta name="author" content="Codrops" />
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" type="text/css" href="../../../../css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="../../../../css/demo.css" />
    <link rel="stylesheet" type="text/css" href="../../../../css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="../../../../css/style.css" />
    <script type="text/javascript" src="../../../../js/modernizr.custom.53451.js"></script>
</head>
<body>
<div class="container">
    <header>
        <h1>CRUD SYSTEM<span> BY  Jalish Mahmud</span></h1>
        <nav class="codrops-demos">
            <a class="current-demo" href="index.html">Home</a>
            <a href="">Birthday</a>
            <a href="">Summary</a>
            <a href="">Email</a>
            <a href="">Profile</a>
            <a href="">Gender</a>
            <a href="">Terms</a>
            <a href="">Hobby</a>
            <a href="">City</a>
            <a href="">Slider</a>
        </nav>
    </header>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="wrapper">
                <h2 >Your Single Book</h2>
                <a class="btn btn-primary btn-xs page_link" href="show.php">Show All Book</a>
                <div class="table_wrap">
                    <table class="table table-striped ">
                        <tr class="warning">
                            <td class="bold_item">ID</td>
                            <td class="bold_item">BOOKS</td>
                            <td class="bold_item">CRECTED AT</td>
                            <td class="bold_item">UPDATED AT</td>
                        </tr>
                        <tr>
                            <td><?php echo $singleBook['id'] ;?></td>
                            <td><?php echo $singleBook['title'] ;?></td>
                            <td><?php echo $singleBook['created_at'] ;?></td>
                            <td><?php echo $singleBook['updated_at'] ;?></td>
                        </tr>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

