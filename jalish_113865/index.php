<!DOCTYPE html>
<html lang="en">
    <head>
		<meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
        <title>CRUD | Jalish</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta name="description" content="3D Gallery with CSS3 and jQuery" />
        <meta name="keywords" content="3d, gallery, jquery, css3, auto, slideshow, navigate, mouse scroll, perspective" />
        <meta name="author" content="Codrops" />
        <link rel="shortcut icon" href="../favicon.ico"> 
        <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
        <link rel="stylesheet" type="text/css" href="css/demo.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
		<script type="text/javascript" src="js/modernizr.custom.53451.js"></script>
    </head>
    <body>
        <div class="container">
			<header>
				<h1>CRUD SYSTEM<span> BY  Jalish Mahmud</span></h1>
				<nav class="codrops-demos">
					<a class="current-demo" href="index.html">Home</a>
					<a href="views/bitm/seip113865/Book/create.php">Book</a>
					<a href="views/bitm/seip113865/birthday/create.php">Birthday</a>
					<a href="views/bitm/seip113865/Summary/create.php">Summary</a>
					<a href="views/bitm/seip113865/Email/create.php">Email</a>
					<a href="views/bitm/seip113865/profilepic/create.php">Profile</a>
					<a href="views/bitm/seip113865/Gender/create.php">Gender</a>
					<a href="views/bitm/seip113865/Terms_conditions/create.php">Terms</a>
					<a href="views/bitm/seip113865/Hobby/create.php">Hobby</a>
					<a href="views/bitm/seip113865/city/create.php">City</a>
					<a href="views/bitm/seip113865/slider/create.php">Slider</a>
				</nav>
			</header>
			<section id="dg-container" class="dg-container">
				<div class="dg-wrapper">
					<a href="#"><img src="images/1.jpg" alt="image01"></a>
					<a href="#"><img src="images/2.jpg" alt="image02"></a>
					<a href="#"><img src="images/3.jpg" alt="image03"></a>
					<a href="#"><img src="images/4.jpg" alt="image04"></a>
					<a href="#"><img src="images/5.jpg" alt="image05"></a>
					<a href="#"><img src="images/6.jpg" alt="image06"></a>
					<a href="#"><img src="images/7.jpg" alt="image07"></a>
					<a href="#"><img src="images/8.jpg" alt="image08"></a>
					<a href="#"><img src="images/9.jpg" alt="image09"></a>
					<a href="#"><img src="images/10.jpg" alt="image10"></a>
					<a href="#"><img src="images/11.jpg" alt="image11"></a>
					<a href="#"><img src="images/12.jpg" alt="image12"></a>
				</div>
				<nav>	
					<span class="dg-prev">&lt;</span>
					<span class="dg-next">&gt;</span>
				</nav>
			</section>
        </div>
		<div class="container">
			<div class="row">
				<h2 class="protect_title" >My all projects</h2>
				<p class="protect_title" ><span style=" color:#000;">Hi there!</span> Please fine my all porjects are given below</p>
				<div class="col-lg-3 col-md-3">
					<div class="single_service">
						<div class="single_item_info">
							<i class="fa fa-book"></i>
							<h2><a href="">Book Title</a></h2>
						</div>
						<div class="single_item_hover_info">
							<i class="fa fa-book"></i>
							<h2><a href="views/bitm/seip113865/Book/show.php">Book Title</a></h2>
							<a class="add" href="views/bitm/seip113865/Book/create.php">Add new book</a> <br/>
							<a class="add" href="views/bitm/seip113865/Book/show.php">Show all books</a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3">
					<div class="single_service">
						<div class="single_item_info">
							<i class="fa fa-birthday-cake"></i>
							<h2><a href="">Your Birthday</a></h2>
						</div>
						<div class="single_item_hover_info">
							<i class="fa fa-birthday-cake"></i>
							<h2><a href="views/bitm/seip113865/birthday/show.php">YOur Birthday</a></h2>
							<a class="add" href="views/bitm/seip113865/birthday/create.php">Add new birthday</a> <br/>
							<a class="add" href="views/bitm/seip113865/birthday/show.php">Show all birthday</a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3">
					<div class="single_service">
						<div class="single_item_info">
							<i class="fa fa-pencil-square-o"></i>
							<h2><a href="">The Summary</a></h2>
						</div>
						<div class="single_item_hover_info">
							<i class="fa fa-pencil-square-o"></i>
							<h2><a href="views/bitm/seip113865/Summary/show.php">The Summary</a></h2>
							<a class="add" href="views/bitm/seip113865/Summary/create.php">Add new summary</a> <br/>
							<a class="add" href="views/bitm/seip113865/Summary/show.php">Show all summary</a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3">
					<div class="single_service">
						<div class="single_item_info">
							<i class="fa fa-envelope-o"></i>
							<h2><a href="">Email Subscription</a></h2>
						</div>
						<div class="single_item_hover_info">
							<i class="fa fa-envelope-o"></i>
							<h2><a href="views/bitm/seip113865/Email/show.php">Email Subscription</a></h2>
							<a class="add" href="views/bitm/seip113865/Email/create.php">Add new email</a> <br/>
							<a class="add" href="views/bitm/seip113865/Email/show.php">Show all email</a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3">
					<div class="single_service">
						<div class="single_item_info">
							<i class="fa fa-user"></i>
							<h2><a href="">Profile Picture</a></h2>
						</div>
						<div class="single_item_hover_info">
							<i class="fa fa-user"></i>
							<h2><a href="views/bitm/seip113865/profilepic/show.php">Profile Picture</a></h2>
							<a class="add" href="views/bitm/seip113865/profilepic/create.php">Add new profile picture</a> <br/>
							<a class="add" href="views/bitm/seip113865/profilepic/show.php">Show all profile picture</a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3">
					<div class="single_service">
						<div class="single_item_info">
							<i class="fa fa-users"></i>
							<h2><a href="">Your Gender</a></h2>
						</div>
						<div class="single_item_hover_info">
							<i class="fa fa-users"></i>
							<h2><a href="views/bitm/seip113865/Gender/show.php"> your Gender</a></h2>
							<a class="add" href="views/bitm/seip113865/Gender/create.php">Add new gender</a> <br/>
							<a class="add" href="views/bitm/seip113865/Gender/show.php">Show all gender</a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3">
					<div class="single_service">
						<div class="single_item_info">
							<i class="fa fa-check-square"></i>
							<h2><a href="">Terms & Condition</a></h2>
						</div>
						<div class="single_item_hover_info">
							<i class="fa fa-check-square"></i>
							<h2><a href="views/bitm/seip113865/Terms_conditions/show.php">Terms & Condition</a></h2>
							<a class="add" href="views/bitm/seip113865/Terms_conditions/create.php">Add new terms</a> <br/>
							<a class="add" href="views/bitm/seip113865/Terms_conditions/show.php">Show all terms</a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3">
					<div class="single_service">
						<div class="single_item_info">
							<i class="fa fa-modx"></i>
							<h2><a href="">Your Hobby</a></h2>
						</div>
						<div class="single_item_hover_info">
							<i class="fa fa-modx"></i>
							<h2><a href="views/bitm/seip113865/Hobby/show.php">Your Hobby</a></h2>
							<a class="add" href="views/bitm/seip113865/Hobby/create.php">Add new hobby</a> <br/>
							<a class="add" href="views/bitm/seip113865/Hobby/show.php">Show all hobby</a>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6">
					<div class="single_service">
						<div class="single_item_info">
							<i class="fa fa-building-o"></i>
							<h2><a href="">Your City</a></h2>
						</div>
						<div class="single_item_hover_info">
							<i class="fa fa-building-o"></i>
							<h2><a href="views/bitm/seip113865/city/show.php">Your City</a></h2>
							<a class="add" href="views/bitm/seip113865/city/create.php">Add new city</a> <br/>
							<a class="add" href="views/bitm/seip113865/city/show.php">Show all city</a>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6">
					<div class="single_service">
						<div class="single_item_info">
							<i class="fa fa-sliders"></i>
							<h2><a href="">Image Slider</a></h2>
						</div>
						<div class="single_item_hover_info">
							<i class="fa fa-sliders"></i>
							<h2><a href="views/bitm/seip113865/slider/show.php">Image Slider</a></h2>
							<a class="add" href="views/bitm/seip113865/slider/create.php">Add new slider</a> <br/>
							<a class="add" href="views/bitm/seip113865/slider/show.php">Show all slider</a>
						</div>
					</div>
				</div>
				
			</div>
		</div>
		
		
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.gallery.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<script type="text/javascript">
			$(function() {
				$('#dg-container').gallery();
			});
		</script/>
    </body>
</html>