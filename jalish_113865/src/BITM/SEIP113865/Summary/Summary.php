<?php
namespace App\BITM\SEIP113865\Summary;
class Summary{
    public $id = '';
    public $title = '';
    public $conn = '';

    public function __construct(){
        $this->conn = mysqli_connect("localhost", "root", "", "allprojects113865");
    }
    public function store($data = ''){
        session_start();
        $query = "INSERT INTO `summary`(`id`, `title`) VALUE(NULL, '".$data['book']."')";
        if(isset($data['book']) && !empty($data['book'])){
            if($this->conn->query($query)){
                $_SESSION['msg'] = 'Data Successfully saved';
                header('location:create.php');
            }
            else{
                $_SESSION['msg'] = 'Sorry, Unable to saved data';
                header('location:crate.php');
            }
        }
    }
    public function index(){
        $query = "SELECT * FROM `summary` where `deleted_at` is NULL ";

        $data = $this->conn->query($query);
        $arr = array();
        while($onedata = mysqli_fetch_assoc($data)){
            $arr[] = $onedata;
        }
        return $arr;
    }
    public function single($id = ""){
        $query = "select * from `summary` WHERE `id` = '".$id."' ";
        $data = $this->conn->query($query);
        $onedata = mysqli_fetch_assoc($data);
        return $onedata;
    }
    public function update($data= ''){
        session_start();
        $query = "update `summary` set `title` = '".$data['book']."' where id = '".$data['id']."' ";
        if(isset($data['book']) && !empty($data['book'])){
            if($this->conn->query($query)){
                $_SESSION['msg'] = "Data Successfully Updated";
                header('location:show.php');
            }
            else{
                $_SESSION['msg'] = 'Data not Updated';
                header('location:edit.php');
            }
        }
    }
    public function delete($id = ''){
        session_start();
        $query = "delete from `summary` where `id` = '".$id."'";
        if($this->conn->query($query)){
            $_SESSION['msg'] = 'Data has been deleted';
            header('location:bin.php');
        }
        else{
            $_SESSION['msg'] = 'Data not deleted yet';
            header('location:bin.php');
        }


    }
    public function trash($id= ''){
        session_start();
        $query = "update `summary` set `deleted_at` = '".date("Y-m-d")."' where id = '".$id."' ";

            if($this->conn->query($query)){
                $_SESSION['msg'] = "Data Successfully Deleted";
                header('location:show.php');
            }
            else{
                $_SESSION['msg'] = 'Data not Deleted';
                header('location:show.php');
            }
    }
    public function bin(){
        $query = "SELECT * FROM `summary` where `deleted_at` is not NULL ";

        $data = $this->conn->query($query);
        $arr = array();
        while($onedata = mysqli_fetch_assoc($data)){
            $arr[] = $onedata;
        }
        return $arr;
    }
    public function recover($id= ''){
        session_start();
        $query = "update `summary` set `deleted_at` = NULL where id = '".$id."' ";

        if($this->conn->query($query)){
            $_SESSION['msg'] = "Data Successfully Recovered";
            header('location:bin.php');
        }
        else{
            $_SESSION['msg'] = 'Data not Recovered';
            header('location:bin.php');
        }
    }
}