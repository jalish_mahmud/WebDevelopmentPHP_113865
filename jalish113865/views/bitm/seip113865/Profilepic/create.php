<?php 
session_start();

?>


<!DOCTYPE HTML>
<!--
	Visualize by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>Profile Picture</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body>

		<!-- Wrapper -->
			<div id="wrapper" style="margin-top: 100px; ">
			
			

	<div id="middle">
	
	<?php 
		if(!empty($_SESSION['msg'])){
			echo $_SESSION['msg'];
			unset($_SESSION['msg']);
		}
	?>
	
	<h2> User's Profile </h2>
	<form action="store.php" method="post" enctype="multipart/form-data">
		<span> Name: </span>
		<input type="text" name="title" /><br><br>
		
		<span> Profile Picture: </span>
		<input type="file" name="profilepic" /><br><br>
		
		<input type="submit" value="Upload" />
	</form>
	
	<a href="index.php">Go to home</a>
	
	<!-- Footer -->


			</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.poptrox.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>