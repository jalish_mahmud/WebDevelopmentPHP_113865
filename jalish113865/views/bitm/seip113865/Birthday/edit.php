<?php

session_start();


include_once '../../../../vendor/autoload.php';

use App\bitm\seip113865\Birthday\Birthday;

$ob = new Birthday();

if(empty($_SESSION['id']))
{
    $id = $_GET['id'];

}
else
{
    $id = $_SESSION['id'];
    unset($_SESSION['id']);
}




$data = $ob->single($id);




?>




<!DOCTYPE HTML>
<!--
    Visualize by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->

<title>Profile Picture</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="stylesheet" href="assets/css/main.css" />
</head>
<body>

<!-- Wrapper -->
<div id="wrapper" style="margin-top: 100px; ">




    <h2>your Birthday</h2>

    <?php

    if(!empty($_SESSION['msg']))
    {
        echo $_SESSION['msg'];
        unset($_SESSION['msg']);
    }
    ?>


    <form action="update.php" method="post">
        Name: <input type="text" name="title" value="<?php echo $data['title']; ?>" /><br><br>
        Date Of Birth: <input type="text" name="birthday" value="<?php echo date("d-m-Y", strtotime($data['birthday'])) ; ?>" /><br><br>
        <input type="hidden" name="id" value="<?php echo $id; ?>" />
        <input type="submit" value="Update" />
    </form>
    <a href="index.php">Back to Home</a>






</div>

<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.poptrox.min.js"></script>
<script src="assets/js/skel.min.js"></script>
<script src="assets/js/main.js"></script>



</body>
</html>
