<?php

namespace App\bitm\seip113865\Checkbox;

Class Checkbox
{
   public $id = "";
   public $title = "";
   public $conn = "";

   public function __construct() {
      $this->conn = mysqli_connect("localhost", "root", "", "checkbox");
   }


   public function store($data = "")
   {
      session_start();
      $query = " INSERT INTO `checkbox`(`id`, `title`,`checkbox`) VALUES (null, '".$data['title']."', '".$data['checkbox']."') ";
      if(!empty($data['title']) && !empty($data['checkbox']) ) {
         if ($this->conn->query($query)) {
            $_SESSION['message'] = "successfully Added";
            header("location:create.php");
         } else {
            $_SESSION['message'] = "successfully Not Added";
            header("location:create.php");
         }
      }
      else{
         $_SESSION['message'] = "Please! Fill all the fields properly";
         header("location:create.php");
      }
   }

   public function index()
   {
       $query = " SELECT * FROM `checkbox` where `deleted_at` is null ";

       $data = $this->conn->query($query);
       $dt = array();

      while($result = mysqli_fetch_assoc($data))
      {
         $dt[] = $result;
      }

      return $dt;

   }

   public function bin()
   {
      $query = " SELECT * FROM `checkbox` where `deleted_at` is not null ";

      $data = $this->conn->query($query);
      $dt = array();

      while($result = mysqli_fetch_assoc($data))
      {
         $dt[] = $result;
      }

      return $dt;

   }


   public function single($id = "")
   {
       $query = " SELECT * FROM `checkbox` where `id`= '".$id."' ";

       $data = $this->conn->query($query);


         $result = mysqli_fetch_assoc($data);


      return $result;

   }

   public function delete($id="")
   {
      $query = " DELETE FROM `checkbox` where `id`= '".$id."' ";

       if($this->conn->query($query))
      {
         $_SESSION['message'] = "Data successfully Deleted";
         header("location:bin.php");
      }else
      {
         $_SESSION['message'] = "Data couldn't be Added";
          header("location:bin.php");
      }

   }

   public function update($data="")
   {
      session_start();
      $query = " UPDATE `checkbox` SET `title`='".$data['title']."', `checkbox` = '".$data['checkbox']."'  where `id`= '".$data['id']."' ";

         if ($this->conn->query($query)) {
            $_SESSION['message'] = "Data successfully Updated";
            header("location:show.php");
         } else {
            $_SESSION['message'] = "Data couldn't be Updated";
            header("location:show.php");
         }

      

   }

   public function trash($id = "")
   {
      $query = " UPDATE `checkbox` SET `deleted_at` = '".date("Y-m-d")."'  where `id`= '".$id."' ";
      $this->conn->query($query);
      header("location:show.php");
   }

   public function recover($id = "")
   {
      $query = " UPDATE `checkbox` SET `deleted_at` = null  where `id`= '".$id."' ";
      $this->conn->query($query);
      header("location:bin.php");
   }



}

