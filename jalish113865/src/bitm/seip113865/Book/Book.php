<?php

namespace App\bitm\seip113865\Book;

Class Book
{
   public $id = "";
   public $title = "";
   public $conn = "";

   public function __construct() {
      $this->conn = mysqli_connect("localhost", "root", "", "allproject");
   }


   public function store($data = "")
   {
      session_start();
      $query = " INSERT INTO `book`(`id`, `title`,`book`) VALUES (null, '".$data['title']."', '".$data['book']."') ";
      if(!empty($data['title']) && !empty($data['book']) ) {
         if ($this->conn->query($query)) {
            $_SESSION['message'] = "successfully Added";
            header("location:create.php");
         } else {
            $_SESSION['message'] = "successfully Not Added";
            header("location:create.php");
         }
      }
      else{
         $_SESSION['message'] = "Please! Fill all the fields";
         header("location:create.php");
      }
   }

   public function index()
   {
       $query = " SELECT * FROM `book` where `deleted_at` is null ";

       $data = $this->conn->query($query);
       $dt = array();

      while($result = mysqli_fetch_assoc($data))
      {
         $dt[] = $result;
      }

      return $dt;

   }

   public function bin()
   {
      $query = " SELECT * FROM `book` where `deleted_at` is not null ";

      $data = $this->conn->query($query);
      $dt = array();

      while($result = mysqli_fetch_assoc($data))
      {
         $dt[] = $result;
      }

      return $dt;

   }


   public function single($id = "")
   {
       $query = " SELECT * FROM `book` where `id`= '".$id."' ";

       $data = $this->conn->query($query);


         $result = mysqli_fetch_assoc($data);


      return $result;

   }

   public function delete($id="")
   {
      $query = " DELETE FROM `book` where `id`= '".$id."' ";

       if($this->conn->query($query))
      {
         $_SESSION['message'] = "Data successfully Deleted";
         header("location:bin.php");
      }else
      {
         $_SESSION['message'] = "Data cann't be Added";
          header("location:bin.php");
      }

   }

   public function update($data="")
   {
      session_start();
      $query = " UPDATE `book` SET `title`='".$data['title']."', `book` = '".$data['book']."'  where `id`= '".$data['id']."' ";

      if(!empty($data['title']) && !empty($data['book']) ) {
         if ($this->conn->query($query)) {
            $_SESSION['message'] = "Data successfully Updated";
            header("location:show.php");
         } else {
            $_SESSION['message'] = "Data cann't be Updated";
            header("location:show.php");
         }
      }else
      {
         $_SESSION['message'] = "Input field shouldn't be empty!";
         $_SESSION['id'] = $data['id'];
         header("location:edit.php");
      }

      

   }

   public function trash($id = "")
   {
      $query = " UPDATE `book` SET `deleted_at` = '".date("Y-m-d")."'  where `id`= '".$id."' ";
      $this->conn->query($query);
      header("location:show.php");
   }

   public function recover($id = "")
   {
      $query = " UPDATE `book` SET `deleted_at` = null  where `id`= '".$id."' ";
      $this->conn->query($query);
      header("location:bin.php");
   }



}



