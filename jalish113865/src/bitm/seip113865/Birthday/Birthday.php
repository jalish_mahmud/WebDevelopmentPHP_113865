<?php

namespace App\bitm\seip113865\birthday;

Class Birthday
{
   public $id = "";
   public $title = "";
   public $conn = "";
   
   public function __construct() {
      $this->conn = mysqli_connect("localhost", "root", "", "allproject");
   }
   


   public function store($data = "")
   {
      session_start();
      
    
      
      $query = " INSERT INTO `birthday`(`id`, `title`,`birthday`) VALUES (null, '".$data['title']."', '".date('Y-m-d',strtotime($data['birthday']))."') ";
      if(!empty($data['title']) && !empty($data['birthday']) ) {
         if ($this->conn->query($query)) {
            $_SESSION['message'] = "Data successfully Added";
            header("location:create.php");
         } else {
            $_SESSION['message'] = "Data couldn't be Added";
            header("location:create.php");
         }

      }else
      {
         $_SESSION['message'] = "Input field's may be empty";
         header("location:create.php");

      }


   }
   
   public function index()
   {
       $query = " SELECT * FROM `birthday` where `deleted_at` is null ";

       $data = $this->conn->query($query);
       $dt = array();

      while($result = mysqli_fetch_assoc($data))
      {
         $dt[] = $result;
      }

      return $dt;

   }



   public function bin()
   {
      $query = " SELECT * FROM `birthday` where `deleted_at` is not null ";

      $data = $this->conn->query($query);
      $dt = array();

      while($result = mysqli_fetch_assoc($data))
      {
         $dt[] = $result;
      }

      return $dt;

   }


   public function single($id = "")
   {
       $query = " SELECT * FROM `birthday` where `id`= '".$id."' ";
      
       $data = $this->conn->query($query);
      
       
         $result = mysqli_fetch_assoc($data);
     
      
      return $result;
      
   }
   
   public function delete($id="")
   {
      $query = " DELETE FROM `birthday` where `id`= '".$id."' ";
             
       if($this->conn->query($query))
      {
         $_SESSION['message'] = "Data successfully Deleted";
         header("location:bin.php");
      }else
      {
         $_SESSION['message'] = "Data couldn't be Added";
          header("location:bin.php");
      }
       
   }

   public function update($data="")
   {
      session_start();

      $query = " UPDATE `birthday` SET `title`= '".$data['title']."', `birthday` = '".date("Y-m-d",strtotime($data['birthday']))."'  where `id`= '".$data['id']."' ";

   if(!empty($data['title']) &&  !empty($data['birthday']) )
   {
      if($this->conn->query($query))
      {
         $_SESSION['message'] = "Data successfully Updated";
         header("location:show.php");
      }else
      {
         $_SESSION['message'] = "Data couldn't be Updated";
         header("location:show.php");
      }
   }
      else
      {
         $_SESSION['id'] = $data['id'];
         $_SESSION['message'] = "Input field shouldn't be empty!";
         header("location:edit.php");
      }


   }

   public function trash($id = "")
   {
      $query = " UPDATE `birthday` SET `deleted_at` = '".date("Y-m-d")."'  where `id`= '".$id."' ";
      $this->conn->query($query);
      header("location:show.php");
   }

   public function recover($id = "")
   {
      $query = " UPDATE `birthday` SET `deleted_at` = null  where `id`= '".$id."' ";
      $this->conn->query($query);
      header("location:bin.php");
   }



}

