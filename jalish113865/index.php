<?php
session_start();

error_reporting(E_ALL ^ E_DEPRECATED);
include_once 'vendor/autoload.php';

use App\bitm\seip113865\Profilepic\Profilepic;

$ob = new Profilepic();

$data = $ob->index();



?>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> 
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en"> 
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <title>Genius - HTML5 Website Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
<!--
Genius Template
http://www.templatemo.com/tm-402-genius
-->
    <meta name="author" content="templatemo">
    <meta charset="UTF-8">
    <link href='http://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700,800' rel='stylesheet' type='text/css'>
    
    <!-- CSS Bootstrap & Custom -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="css/templatemo_misc.css">
    <link rel="stylesheet" href="css/animate.css">
    <link href="css/templatemo_style.css" rel="stylesheet" media="screen">
    
    <!-- Favicons -->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    
    <!-- JavaScripts -->
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/modernizr.js"></script>
    <!--[if lt IE 8]>
	<div style=' clear: both; text-align:center; position: relative;'>
            <a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" alt="" /></a>
        </div>
    <![endif]-->
</head>
<body>
    
    
    <div class=""></div>
    <div class="overlay-bg"></div>



    <!-- This one in here is responsive menu for tablet and mobiles -->


    <div class="main-content">
        <div class="container">
            <div class="row">


                <!-- Begin Content -->
                <div class="col-md-10">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="templatemo_logo">
                                <a href="#">
                                    <h2>Jalish_CRUD_113865</h2>
                                </a>
                            </div> <!-- /.logo -->
                        </div> <!-- /.col-md-12 -->
                    </div> <!-- /.row -->

							<div class="content-inner">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3 style="width:400px; text-align:center; margin:0 auto;" class="widget-title">Please find your project</h3>
                                    </div> <!-- /.col-md-12 -->
                                </div> <!-- /.row -->
                                <div class="row services">
                                    <div class="col-md-4 col-sm-6">
                                        <div class="service">
                                            <div class="header">
                                                <div class="header-bg"></div>
                                                <div class="service-header">
                                                    <div class="icon">
                                                        <i class="fa fa-book"></i>
                                                    </div>
                                                    <h4 class="service-title">Book Project</h4>
                                                </div>
                                            </div>
                                            <div class="body" style="text-align:center; font-size:20px; ">
                                            	<a href="views/bitm/seip113865/book/index.php">Go to Book Project</a>
                                            </div>
                                        </div>
                                    </div> <!-- /.col-md-4 -->
                                    <div class="col-md-4 col-sm-6">
                                        <div class="service">
                                            <div class="header">
                                                <div class="header-bg"></div>
                                                <div class="service-header">
                                                    <div class="icon">
                                                       <i class="fa fa-birthday-cake"></i>
                                                    </div>
                                                    <h4 class="service-title">Birthday</h4>
                                                </div>
                                            </div>
                                           <div class="body" style="text-align:center; font-size:20px; ">
                                            	<a href="views/bitm/seip113865/Birthday/index.php">Go to Birthday Project</a>
                                            </div>
                                           
                                        </div>
                                    </div> <!-- /.col-md-4 -->
                                    <div class="col-md-4 col-sm-6">
                                        <div class="service">
                                            <div class="header">
                                                <div class="header-bg"></div>
                                                <div class="service-header">
                                                    <div class="icon">
                                                        <i class="fa fa-money"></i>
                                                    </div>
                                                    <h4 class="service-title">Email Project</h4>
                                                </div>
                                            </div>
                                            <div class="body" style="text-align:center; font-size:20px; ">
                                            	<a href="views/bitm/seip113865/email/index.php">Go to Email Project</a>
                                            </div>
                                        </div>
                                    </div> <!-- /.col-md-4 -->
                                    <div class="col-md-4 col-sm-6">
                                        <div class="service">
                                            <div class="header">
                                                <div class="header-bg"></div>
                                                <div class="service-header">
                                                    <div class="icon">
                                                        <i class="fa fa-eye"></i>
                                                    </div>
                                                    <h4 class="service-title">Profile Picture</h4>
                                                </div>
                                            </div>
                                            
                                             <div class="body" style="text-align:center; font-size:20px; ">
                                            	<a href="views/bitm/seip113865/profilepic/index.php">Go to Profile Picture Project</a>
                                            </div>
                                            
                                            
                                        </div>
                                    </div> <!-- /.col-md-4 -->
                                   
                                   
                                    <div class="col-md-4 col-sm-6">
                                        <div class="service">
                                            <div class="header">
                                                <div class="header-bg"></div>
                                                <div class="service-header">
                                                    <div class="icon">
                                                        <i class="fa fa-mobile-phone"></i>
                                                    </div>
                                                    <h4 class="service-title">City</h4>
                                                </div>
                                            </div>
                                            
                                              <div class="body" style="text-align:center; font-size:20px; ">
                                            	<a href="views/bitm/seip113865/city/index.php">Go to City Project</a>
                                            </div>
                                         </div>
                                    </div> <!-- /.col-md-4 -->
                                    
                                    
                                    <div class="col-md-4 col-sm-6">
                                        <div class="service">
                                            <div class="header">
                                                <div class="header-bg"></div>
                                                <div class="service-header">
                                                    <div class="icon">
                                                        <i class="fa fa-mobile-phone"></i>
                                                    </div>
                                                    <h4 class="service-title">Gender</h4>
                                                </div>
                                            </div>
                                            
                                              <div class="body" style="text-align:center; font-size:20px; ">
                                            	<a href="views/bitm/seip113865/gender/index.php">Go to Gender Project</a>
                                            </div>
                                         </div>
                                    </div> <!-- /.col-md-4 -->



                                   <div class="col-md-4 col-sm-6">
                                        <div class="service">
                                            <div class="header">
                                                <div class="header-bg"></div>
                                                <div class="service-header">
                                                    <div class="icon">
                                                        <i class="fa fa-mobile-phone"></i>
                                                    </div>
                                                    <h4 class="service-title">Favourite Actor</h4>
                                                </div>
                                            </div>
                                            
                                              <div class="body" style="text-align:center; font-size:20px; ">
                                            	<a href="views/bitm/seip113865/actor/index.php">Go to Favourite Actor Project</a>
                                            </div>
                                         </div>
                                    </div> <!-- /.col-md-4 -->
                                    
              
                                   <div class="col-md-4 col-sm-6">
                                        <div class="service">
                                            <div class="header">
                                                <div class="header-bg"></div>
                                                <div class="service-header">
                                                    <div class="icon">
                                                        <i class="fa fa-mobile-phone"></i>
                                                    </div>
                                                    <h4 class="service-title">Terms & Condition</h4>
                                                </div>
                                            </div>
                                            
                                              <div class="body" style="text-align:center; font-size:20px; ">
                                            	<a href="views/bitm/seip113865/checkbox/index.php">Go to Terms & Condition Project</a>
                                            </div>
                                         </div>
                                    </div> <!-- /.col-md-4 -->
      
                                   <div class="col-md-4 col-sm-6">
                                        <div class="service">
                                            <div class="header">
                                                <div class="header-bg"></div>
                                                <div class="service-header">
                                                    <div class="icon">
                                                        <i class="fa fa-mobile-phone"></i>
                                                    </div>
                                                    <h4 class="service-title">Sammary</h4>
                                                </div>
                                            </div>
                                            
                                              <div class="body" style="text-align:center; font-size:20px; ">
                                            	<a href="views/bitm/seip113865/summary/index.php">Go to Summary Project</a>
                                            </div>
                                         </div>
                                    </div> <!-- /.col-md-4 -->
                
       
                                 </div> <!-- /.row -->
                            </div> <!-- /.content-inner -->

                    <div id="menu-container" >

                        <div id="menu-4" class="content">
                            <div class="page-header">
                                <h2 class="page-title">Our Services</h2>
                            </div> <!-- /.page-header -->
                           
                        </div> <!-- /.services -->

                        <div id="menu-5" class="content">
                            <div class="page-header">
                                <h2 class="page-title">Stay In Touch</h2>
                            </div> <!-- /.page-header -->
                            <div class="content-inner">
                                <div class="row">
                                    <div class="col-md-8">
                                        <h3 class="widget-title">Send a message</h3>
                                        <p>Feel free to send us a message regarding this Genius HTML5 template. Pellentesque pulvinar, orci vel scelerisque suscipit, libero justo laoreet felis, ac consectetur est nisi quis ligula. Maecenas nec felis elit.</p>
                                        <div class="row contact-form">
                                            <div class="col-md-4">
                                                <label for="name-id">Your Name:</label>
                                                <input name="name-id" type="text" id="name-id" maxlength="40">
                                            </div>
                                            <div class="col-md-4">
                                                <label for="email-id">E-mail:</label>
                                                <input name="email-id" type="text" id="email-id" maxlength="40">
                                            </div>
                                            <div class="col-md-4">
                                                <label for="subject-id">Subject:</label>
                                                <input name="subject-id" type="text" id="subject-id" maxlength="60">
                                            </div>
                                        </div> <!-- /.contact-form -->
                                        <p class="full-row">
                                            <label for="message">Message:</label>
                                            <textarea name="message" id="message" rows="6"></textarea>
                                        </p>
                                        <input class="mainBtn" type="submit" name="" value="Send Message">
                                    </div> <!-- /.col-md-8 -->
                                    <div class="col-md-4">
                                        <div class="information">
                                            <h3 class="widget-title">Information</h3>
                                            <ul class="our-location">
                                                <li><span><i class="fa fa-map-marker"></i>Address:</span>120 Nullam viverra dolor</li>
                                                <li><span><i class="fa fa-map-marker"></i>Phone:</span>010-020-0210</li>
                                                <li><span><i class="fa fa-map-marker"></i>Email:</span><a href="mailto:info@company.com">info@company.com</a></li>
                                            </ul>
                                        </div> <!-- /.information -->
                                        <div class="google-map">
                                            <h3 class="widget-title">Our Location</h3>
                                            <div class="contact-map">
                                                <div class="google-map-canvas" id="map-canvas" style="height: 200px;">
                                                </div>
                                            </div> <!-- /.contact-map -->
                                        </div> <!-- /.google-map -->
                                    </div> <!-- /.col-md-4 -->
                                </div> <!-- /.row -->
                            </div> <!-- /.content-inner -->

                        </div> <!-- /.stay-in-touch -->
                        

                    </div> <!-- /.content-holder -->
                
                </div> <!-- /.col-md-10 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /.main-content -->

    <script src="bootstrap/js/bootstrap.min.js"></script>

    <script src="js/jquery.mixitup.min.js"></script>
    <script src="js/jquery.nicescroll.min.js"></script>
    <script src="js/jquery.lightbox.js"></script>
    <script src="js/templatemo_custom.js"></script>
    <script>
        function initialize() {
          var mapOptions = {
            zoom: 15,
            center: new google.maps.LatLng(16.832179,96.134976)
          };

          var map = new google.maps.Map(document.getElementById('map-canvas'),
              mapOptions);
        }

        function loadScript() {
          var script = document.createElement('script');
          script.type = 'text/javascript';
          script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&' +
              'callback=initialize';
          document.body.appendChild(script);
        }

    </script>
<!-- templatemo 402 genius -->
</body>
</html>

  