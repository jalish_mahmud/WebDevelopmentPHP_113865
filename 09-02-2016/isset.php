<?php

$var = '';
if (isset($var)) {
    echo 'this is set so it will print' . "<br>";
} else {
    echo 'this is not set';
}

$a = "test";
$b = "anothertest";
var_dump(isset($a));
var_dump(isset($a, $b));
unset($a);
var_dump(isset($a));

echo "</br>";
$foo = NULL;
var_dump(isset($foo));
