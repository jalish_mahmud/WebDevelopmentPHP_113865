<?php
    include_once("greeting.php");
    
    use Bitm\Greeting;

    $greeting1 = new Greeting();
    $greeting1->sayHello();
    
    $greeting2 = new Greeting();
    $greeting2->sayHello();
?>