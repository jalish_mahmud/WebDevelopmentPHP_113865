<?php
    //The array_replace() function replaces the values of the first array with the values from following arrays.
    $arr1 = array('jalish', 'danish');
    $arr2 = array('hafsa', 'tamanna');
    $output = array_replace($arr1, $arr2);
    print_r($output);
    //Array ( [0] => hafsa [1] => tamanna ) 
?>
