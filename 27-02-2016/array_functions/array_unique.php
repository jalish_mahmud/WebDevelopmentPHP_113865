<?php
    //The array_unique() function removes duplicate values from an array. If two or more array values are the same, the first appearance will be kept and the other will be removed.\
    $arr = array("a"=>"jalish", "b"=>"danish", "c"=>"jalish");
    print_r(array_unique($arr));
    //Array ( [a] => jalish [b] => danish ) 
?>