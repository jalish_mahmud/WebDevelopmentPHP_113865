<?php
//The array_walk() function runs each array element in a user-defined function. The array's keys and values are parameters in the function.
    function arrayfunctions($key, $value){
        echo "in this key $key contain the value $value" . "<br>";
    }
    $arr= array("First name"=>"Jalish", "Last name"=>"Mahmud");
    array_walk($arr, "arrayfunctions");
    //in this key Jalish contain the value First name
    //in this key Mahmud contain the value Last name
?>