<?php
    //The array_key_exists() function checks an array for a specified key, and returns true if the key exists and false if the key does not exist.
    $arr = array('Bangladesh'=>'my country', 'India'=>'Your country');
    if(array_key_exists('Bangladesh', $arr)){
        echo "array key exists";
    }
    else{
        echo "array key dosent exists";
    }
    //array key exists
?>
