<?php
    //The array_flip() function flips/exchanges all keys with their associated values in an array.
    $arr = array('a'=>'Apple', 'b'=>'banena');
    $ourput = array_flip($arr);
    print_r($ourput);
    //Array ( [Apple] => a [banena] => b ) 
?>