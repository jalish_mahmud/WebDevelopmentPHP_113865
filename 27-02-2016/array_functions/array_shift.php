<?php
    //The array_shift() function removes the first element from an array, and returns the value of the removed element.
    $arr = array("Jalish", "My name", "Is Jalish");
    echo array_shift($arr). "<br>";
    print_r($arr);
    
    //Jalish
//Array ( [0] => My name [1] => Is Jalish )
?>