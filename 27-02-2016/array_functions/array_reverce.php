<?php
    // The array_reverse() function returns an array in the reverse order.
    $arr = array("a" => "jalish", "b"=>"Danihs");
    print_r(array_reverse($arr));
    //Array ( [b] => Danihs [a] => jalish ) 
?>