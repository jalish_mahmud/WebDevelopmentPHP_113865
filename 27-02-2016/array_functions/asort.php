<?php
//The asort() function sorts an associative array in ascending order, according to the value.
    $age = array("Jalish"=>"25", "Danish"=>"22", "Hafsa"=>"15");
    asort($age);

    foreach($age as $key=> $value){
        echo "key =". $key . ", value =" . $value;
        echo "<br>";
    }
    //key =Hafsa, value =15
    //key =Danish, value =22
    //key =Jalish, value =25
?>