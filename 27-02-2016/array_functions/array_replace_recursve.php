<?php
//The array_replace_recursive() function replaces the values of the first array with the values from following arrays recursively.
    $arr1 = array(array("c"=>"red"), array("a"=>"blue","b"=>"white"));
    $arr2 = array(array("c"=>"pink"), array("a"=>"yellow"));
   echo "<pre>";
    print_r(array_replace_recursive($arr1, $arr2));
   echo "</pre>";
   /*

    Array
(
    [0] => Array
        (
            [c] => pink
        )
    [1] => Array
        (
            [a] => yellow
            [b] => white
        )
) 
    */
?>
