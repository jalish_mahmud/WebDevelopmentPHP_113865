<?php
    //The array_values() function returns an array containing all the values of an array.
    //Tip: The returned array will have numeric keys, starting at 0 and increase by 1.
    $arr =array("jalish"=>"first Name", "Mahmud"=>"Second Name");
    print_r( array_values($arr));
    //Array ( [0] => first Name [1] => Second Name ) 
?>