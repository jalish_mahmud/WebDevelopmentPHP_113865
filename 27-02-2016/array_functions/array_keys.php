<?php
    //The array_keys() function returns an array containing the keys.
    $arr = array('Jalish'=>'First', 'Danish'=>'Second', 'Hafsa'=>'Third', 'Tamanna'=>'Fourth');
    print_r(array_keys($arr));
    //Array ( [0] => Jalish [1] => Danish [2] => Hafsa [3] => Tamanna ) 
?>
