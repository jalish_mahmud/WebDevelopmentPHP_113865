<?php
    //The array_push() function inserts one or more elements to the end of an array.
    $arr =array("one", "two");
    array_push($arr, "three", "four", "five");
    print_r($arr);
    //Array ( [0] => one [1] => two [2] => three [3] => four [4] => five ) 
?>