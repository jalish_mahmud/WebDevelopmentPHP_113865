<?php session_start(); ?>
<?php
use App\bitm\seip113865\book\Book;
include_once '../../../../vendor/autoload.php';

$ins_book = new Book();
$book = $ins_book->show();
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="style.css" />
</head>
<body>
    <?php 
        if(isset($_SESSION['msg'])){
            
            echo $_SESSION['msg'];
            unset($_SESSION['msg']);
        }
    ?>
    <div class="form">
        <div class="wrapper">
            <h3>All Books list</h3>
            <div class="middle">
        <table >
        <tr>
            <th>SL</th>
            <th>Book</th>
            <th>Actions</th>
        </tr>
        <?php
        foreach ($book as $single){
        ?>
        <tr>
            <td><?php echo $single['id']; ?></td>
            <td><?php echo $single['title']; ?></td>
            <td>
                <a href="single.php?id=<?php echo $single['id']; ?>">View</a>
                <a href="edit.php?id=<?php echo $single['id']; ?>">Edit</a>
                <a href="delete.php?id=<?php echo $single['id']; ?>">Delete</a>
            </td>
        </tr>
        <?php } ?>
        </table>
            </div>
        </div>
    </div>
</body>
</html>
