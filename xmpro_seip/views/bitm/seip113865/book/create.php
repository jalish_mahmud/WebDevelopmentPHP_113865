<?php session_start(); ?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="style.css" />
</head>
<body>
    <?php
        if(isset($_SESSION['msg'])){
            echo $_SESSION['msg'];
            unset($_SESSION['msg']);
        }
    ?>
    <div class="form">
        <form action="store.php" method="post">
            <div class="wrapper">
                <h3>Add New Book Item</h3>
                <div class="middle">
                    <p>Book Name: <input type="text" name="book" /></p>
                    <input type="submit" value="SAVE" />
                </div>
            </div>
        </form>
    </div>
</body>
</html>