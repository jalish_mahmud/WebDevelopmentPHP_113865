<?php
use App\bitm\seip113865\birthday\Birthday;
include_once '../../../../vendor/autoload.php';

$ins_book = new Birthday();
$rcv_id = $_GET['id'];
$book = $ins_book->single($rcv_id);
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
    <table border="1">
        <tr>
            <th>SL</th>
            <th>Birth Date</th>
            <th>Created At</th>
            <th>Updated At</th>
        </tr>
        <tr>
            <td><?php echo $book['id'];?></td>
            <td><?php echo $book['title'];?></td>
            <td><?php echo $book['created_at'];?></td>
            <td><?php echo $book['updated_at'];?></td>  
        </tr>
    </table>
</body>
</html>