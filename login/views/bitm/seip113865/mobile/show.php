<?php
use App\bitm\seip113865\mobile\Mobile;
include_once '../../../../vendor/autoload.php';

$mobb = new Mobile;
$mob = $mobb->show();
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
    <table border="1">
        <tr>
            <th>SL</th>
            <th>Brand</th>
            <th>Model</th>
            <th>Actions</th>
        </tr>
        <?php 
        foreach ($mob as $mobile){
        ?>
        <tr>
            <td><?php echo $mobile['id'];?></td>
            <td><?php echo $mobile['title'];?></td>
            <td><?php echo $mobile['model'];?></td>
            <td>
                <a href="single.php?id=<?php echo $mobile['id']; ?>">single</a>
                <a href="edit.php?id=<?php echo $mobile['id']; ?>">update</a>
                <a href="trash.php?id=<?php echo $mobile['id']; ?>">delete</a>
            </td>
        </tr>
        <?php }?>
    </table>
    <a href="index.php">got to home</a>
</body>
</html>