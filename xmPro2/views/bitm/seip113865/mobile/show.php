<?php
    use App\bitm\seip113865\mobile\Mobile;
    include_once '../../../../vendor/autoload.php';
    
    $mobbb = new Mobile();
    $rcv_mob = $mobbb->show();
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
    <table border="1">
        <tr>
            <th>SL</th>
            <th>Model</th>
            <th>Brand</th>
            <th>Action</th>
        </tr>
        <?php
            $sl = 1;
            foreach ($rcv_mob as $mob){    
        ?> 
        <tr>
            <td><?php echo $sl++; ?></td>
            <td><?php echo $mob['title']; ?></td>
            <td><?php echo $mob['model']; ?></td>
            <td>
                <a href="single.php?num=<?php echo $mob['id'];?>">Single</a>
                <a href="edit.php?num=<?php echo $mob['id'];?>">Edit</a>
                <a href="delete.php?num=<?php echo $mob['id'];?>">Delete</a>
            </td>
        </tr>
              <?php } ?>
    </table>
</body>
</html>