<?php
session_start();

use App\bitm\seip113865\mobile\Mobile;
include_once '../../../../vendor/autoload.php';


$moobb = new Mobile;

$mobiles = $moobb->trash();
?>


<html>
<head>

</head>
<body>
    <?php
        if(isset($_SESSION['msg'])){
            echo $_SESSION['msg'];
            unset($_SESSION['msg']);
                    
        }
    ?>
    <table border="1">
        <tr>
            <th>SL</th>
            <th>Brand</th>
            <th>Model</th>
            <th>Actions</th>
        </tr>
        <?php
            $sl = 1;
            foreach ($mobiles as $mob){
        ?>
        <tr>
            <td><?php echo $sl++ ;?></td>
            <td><?php echo $mob['title'];?></td>
            <td><?php echo $mob['model'];?></td>
            <td>
                <a href="recover.php?id=<?php echo $mob['id']?>">Recover</a>
                <a href="delete.php?id=<?php echo $mob['id']?>">Delete</a>
            </td>
        </tr>
        <?php }?>
    </table>
</body>
</html>