<?php

namespace App\bitm\seip113865\mobile;
class Mobile{
    public $link = "";
    
    public function __construct() {
        $this->link = mysqli_connect("localhost", "root", "", "xmpro");
    }
    public function store($data = ""){
        session_start();
        $query = "INSERT INTO `mobile`(`id`, `title`, `model`)VALUES (null, '".$data['brand']."', '".$data['model']."')";
        if(isset($data['brand']) && isset($data['model'])){
            if($this->link->query($query) == true){
                $_SESSION['msg'] = "data added";
            }
            else{
                $_SESSION['msg'] = "data not added";
            }
            header("location:create.php");
        }
    }
    public function show(){
        
        $query = "SELECT * FROM `mobile` WHERE `deleted_at` is NULL";
        if($resource = $this->link->query($query)){
            $arr = array();
            while ($rcv_data = mysqli_fetch_assoc($resource)){
                $arr[] = $rcv_data;
            }
        }
        return $arr;
        
    }
    public function single($id=""){
        $query = "SELECT * FROM `mobile` where `id` = '".$id."'";
        $resource = $this->link->query($query);
        $return_resource = mysqli_fetch_assoc($resource);
        return $return_resource ;
    }
    public function update($data = "" ){
        session_start();

        
        $query = "UPDATE `mobile` SET  `title` = '".$data['brand']."' , `model` = '".$data['model']."' where `id`= '".$data['id']."' ";
        if($this->link->query($query) == true){
            $_SESSION['msg'] = "Data updated";
            header("location:show.php");
    }
        else{
            $_SESSION['msg'] = "Data not Updated";
        }
        
    }
    public function delete($id = ""){
        session_start();
        $query = "DELETE from `mobile` where `id` = '".$id."'  " ;
        $this->link->query($query);
        $_SESSION['msg'] = "data deleted";
        header("location:show.php");
    }
    public function trash(){
         $query2 = "SELECT * FROM `mobile` WHERE `deleted_at` is not NULL";
        if($resource = $this->link->query($query2)){
            $arr = array();
            while ($rcv_data = mysqli_fetch_assoc($resource)){
                $arr[] = $rcv_data;
            }
        }
        return $arr;
        
    }
    
    public function bin($id = "")
    {
        $query1 = "UPDATE `mobile` SET   `deleted_at` = '".date("Y-m-d")."' where `id`= '".$id."' ";
        $this->link->query($query1);
        header("location:show.php");
    }
    
     public function recover($id = "")
    {
        $query1 = "UPDATE `mobile` SET   `deleted_at` = null where `id`= '".$id."' ";
        $this->link->query($query1);
        header("location:trash.php");
    }
    
}