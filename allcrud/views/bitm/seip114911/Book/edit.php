<?php

session_start();

include_once '../../../../vendor/autoload.php';

use App\bitm\seip114911\Book\Book;

$bookob = new Book();

if(empty($_SESSION['id']))
{
    $id = $_GET['id'];
}else{
    $id = $_SESSION['id'];
}

$bookdata = $bookob->single($id);


?>









<!DOCTYPE HTML>
<!--
	Visualize by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<head>
    <title>Profile Picture</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="assets/css/main.css" />
</head>
<body>

<!-- Wrapper -->
<div id="wrapper" style="margin-top: 100px; ">



    <?php
    if(!empty($_SESSION['message']))
    {
        echo $_SESSION['message'];
        unset($_SESSION['message']);
    }

    ?>


    <form action="update.php" method="post">
        Book Name: <input type="text" name="title" value="<?php echo $bookdata['title']; ?>" /><br><br>
        Writer Name: <input type="text" name="book" value="<?php echo $bookdata['book']; ?>" /><br><br>
    
        <input type="hidden" name="id" value="<?php echo $id; ?>" />
        <input type="submit" name="submit" value="Update" />
    </form>
    <a href="index.php">Back to Home</a>







    <!-- Footer -->
    <footer id="footer">
        <p>&copy; Nirob. All rights reserved.</p>
    </footer>

</div>

<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.poptrox.min.js"></script>
<script src="assets/js/skel.min.js"></script>
<script src="assets/js/main.js"></script>



</body>
</html>



