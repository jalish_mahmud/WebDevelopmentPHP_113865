<?php
session_start();
include_once '../../../../vendor/autoload.php';

use App\bitm\seip114911\Book\Book;

$bookob = new Book();

$bookdata = $bookob->index();

?>







<!DOCTYPE HTML>
<!--
	Visualize by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<head>
 <title>Profile Picture</title>
 <meta charset="utf-8" />
 <meta name="viewport" content="width=device-width, initial-scale=1" />
 <link rel="stylesheet" href="assets/css/main.css" />
</head>
<body>

<!-- Wrapper -->
<div id="wrapper" style="margin-top: 100px; ">






<h2>All Books</h2>

 <?php
 if(!empty($_SESSION['message'])){
  echo $_SESSION['message'];
  unset($_SESSION['message']);
 }

 ?>

 <table border="1">
            
 <tr>
<td>Sl</td> 
<td>Book Name</td> 
<td>Writer</td>
<td>Action</td>
</tr>
            
    
 <?php
 $sl = 0;
 foreach ($bookdata as $single) { ?>
 <tr>
 <td><?php echo $sl++ ; ?></td> 
 <td><?php echo $single['title'] ; ?></td> 
 <td><?php echo $single['book'] ; ?></td>
 <td>
 <a href="single.php?id=<?php echo $single['id']; ?>">Show</a>|
 <a href="edit.php?id=<?php echo $single['id']; ?>">Edit</a>|
 <a href="trash.php?id=<?php echo $single['id']; ?>">Delete</a>
 </td>
 </tr>
 <?php } ?>
            
            
            
</table>
<a href="index.php">Back to Home </a>






<!-- Footer -->
<footer id="footer">
 <p>&copy; Nirob. All rights reserved.</p>
</footer>

</div>

<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.poptrox.min.js"></script>
<script src="assets/js/skel.min.js"></script>
<script src="assets/js/main.js"></script>



</body>
</html>