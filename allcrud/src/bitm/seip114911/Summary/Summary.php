<?php

namespace App\bitm\seip114911\Summary;

Class Summary
{
    public $id = "";
    public $title = "";
    public $conn = "";

    public function __construct() {
        $this->conn = mysqli_connect("localhost", "root", "", "allproject");
    }


    public function store($data = "")
    {
        session_start();
        $query = " INSERT INTO `summary`(`id`, `title`,`summary`) VALUES (null, '".$data['title']."', '".$data['summary']."') ";
        if(!empty($data['title']) && !empty($data['summary']) ) {
            if ($this->conn->query($query)) {
                $_SESSION['message'] = "successfully Added";
                header("location:create.php");
            } else {
                $_SESSION['message'] = "successfully Not Added";
                header("location:create.php");
            }
        }
        else{
            $_SESSION['message'] = "Please! Fill all the fields";
            header("location:create.php");
        }
    }

    public function index()
    {
        $query = " SELECT * FROM `summary` where `deleted_at` is null ";

        $data = $this->conn->query($query);
        $dt = array();

        while($result = mysqli_fetch_assoc($data))
        {
            $dt[] = $result;
        }

        return $dt;

    }

    public function bin()
    {
        $query = " SELECT * FROM `summary` where `deleted_at` is not null ";

        $data = $this->conn->query($query);
        $dt = array();

        while($result = mysqli_fetch_assoc($data))
        {
            $dt[] = $result;
        }

        return $dt;

    }


    public function single($id = "")
    {
        $query = " SELECT * FROM `summary` where `id`= '".$id."' ";

        $data = $this->conn->query($query);


        $result = mysqli_fetch_assoc($data);


        return $result;

    }

    public function delete($id="")
    {
        $query = " DELETE FROM `summary` where `id`= '".$id."' ";

        if($this->conn->query($query))
        {
            $_SESSION['message'] = "Data successfully Deleted";
            header("location:bin.php");
        }else
        {
            $_SESSION['message'] = "Data cann't be Added";
            header("location:bin.php");
        }

    }

    public function update($data="")
    {
        session_start();
        $query = " UPDATE `summary` SET `title`='".$data['title']."', `summary` = '".$data['summary']."'  where `id`= '".$data['id']."' ";

        if(!empty($data['title']) && !empty($data['summary']) ) {
            if ($this->conn->query($query)) {
                $_SESSION['message'] = "Data successfully Updated";
                header("location:show.php");
            } else {
                $_SESSION['message'] = "Data cann't be Updated";
                header("location:show.php");
            }
        }else
        {
            $_SESSION['message'] = "Input field shouldn't be empty!";
            $_SESSION['id'] = $data['id'];
            header("location:edit.php");
        }



    }

    public function trash($id = "")
    {
        $query = " UPDATE `summary` SET `deleted_at` = '".date("Y-m-d")."'  where `id`= '".$id."' ";
        $this->conn->query($query);
        header("location:show.php");
    }

    public function recover($id = "")
    {
        $query = " UPDATE `summary` SET `deleted_at` = null  where `id`= '".$id."' ";
        $this->conn->query($query);
        header("location:bin.php");
    }



}



