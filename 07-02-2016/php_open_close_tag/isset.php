<?php
    // is_null
    $foo = NULL;
    var_dump(is_null($foo));
    echo "</br>";
// isset
    $var = '';
    if (isset($var)){
        echo "This var is set so i will print";
    }
    echo "</br>";
    $a = "test";
    $b = "anothertest";
    
    var_dump(isset($a));
    var_dump(isset($a, $b));
    echo "</br>";
    unset ($a);
    
    var_dump(isset($a));
    var_dump(isset($a, $b));
    echo "</br>";
    $foo = NULL;
    var_dump (isset($foo));
    echo "</br>";
    
    // print_r
    $a = array('a' => 'apple', 'b' => 'banana', 'c' => array ('x', 'y', 'z') );
    print_r ($a);
    
?>
