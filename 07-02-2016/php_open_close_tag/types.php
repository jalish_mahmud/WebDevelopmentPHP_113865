<?php
    // Boolean:
    $foo = true; // assign the value TRUE to $foo
    
    // Integer:
    echo $a = 1234; // decimal number
    echo "</br>";
    echo $a = -123; // a nagetive number
    echo "</br>";
    echo $a = 0123; // octal number (equivalent to 83 decimal)
    echo "</br>";
    echo $a = 0x1A; // hexadecimal number (equivalent to 26 decimal)
    echo "</br>";
    echo $a = 0b111111111; // binary number (equivalent to 255 decimal)


?>