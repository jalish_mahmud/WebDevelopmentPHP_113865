<?php
include_once '../../../../vendor/autoload.php';
include_once '../../../../vendor/mpdf/mpdf/mpdf.php';
use App\bitm\seip113865\mobile\Mobile;

$mobb = new Mobile;
$mob = $mobb->show();

$trs = "";
$serial = "";
foreach($mob as $mobile):
    $serial++;
        $trs.="<tr>";
        $trs.="<td>".$serial."</td>";
        $trs.="<td>".$mobile['id']."</td>";
         $trs.="<td>".$mobile['title']."</td>";
         $trs.="</tr>";
endforeach;
$html = <<<EOD
        
        <!DOCTYPE HTML>
        <html lang="en-US">
        <head>
            <meta charset="UTF-8">
            <title>List of Mobile</title>
        </head>
        <body>
            <h1>List of Mobile</h1>
        <table border="1">
            <thead>
                <tr>
                    <th>Sl.</th>
                    <th>ID</th>
                    <th>Title</th>
                </tr>
            </thead>
        <tbody>
            $trs;
         </tbody>
        </table>
EOD;

        $mpdf= new mPDF();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
        exit;
?>