<?php
use App\bitm\seip113865\mobile\Mobile;
include_once '../../../../vendor/autoload.php';

$mobb = new Mobile;
$rcv_id = $_GET['id'];
$mobile = $mobb->single($rcv_id);
?>

<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
    <table border="1">
        <tr>
            <th>SL</th>
            <th>Brand</th>
            <th>Model</th>
            <th>Created at</th>
        </tr>
        <tr>
            <th><?php echo $mobile['id'];?></th>
            <th><?php echo $mobile['title'];?></th>
            <th><?php echo $mobile['model'];?></th>
            <th><?php echo $mobile['created_at'];?></th>
        </tr>
    </table>
</body>
</html>