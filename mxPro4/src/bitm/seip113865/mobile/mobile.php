<?php
namespace App\bitm\seip113865\mobile;
class Mobile{
    public $link = "";
    
    public function __construct() {
        $this->link = mysqli_connect("localhost", "root", "", "xmpro1");
    }
    public function store($data = ""){
        session_start();
        $quary = "insert into `mobile`(`id`,`title`,`model`) values(null, '".$data['brand']."', '".$data['model']."')";
        if(isset($data['brand']) && isset($data['model']) && !empty($data['brand']) && !empty($data['model'])){
           if($this->link->query($quary)){
            $_SESSION['msg'] = "data saved";
        }
        else{
            $_SESSION['msg'] = "data not saved";
        } 
        }
        header("location:create.php");
    }
    public function show(){
        $query = "select * from `mobile` WHERE `deleted_at` is NULL";
        if($resource = $this->link->query($query)){
            $arr = array();
            while($rcv_resource = mysqli_fetch_assoc($resource)){
                $arr[] = $rcv_resource;
            }
        }
        return $arr;
        
    }
    public function single($id = ""){
        $query = "select * from `mobile` where `id` = '".$id."'";
        
        $resource = $this->link->query($query);
        $return_result = mysqli_fetch_assoc($resource);
        return $return_result;
    }
    public function update($data= ""){
        $query = "update `mobile` set `title` = '".$data['brand']."', `model` = '".$data['model']."' where `id` = '".$data['id']."'";
        if($this->link->query($query)){
            $_SESSION['msg'] = "data updated";
            header("location:show.php");
        }
    }
    public function delete($id = ""){
        $query = "delete from `mobile` WHERE `id` = '".$id."'";
        $this->link->query($query);
        $_SESSION['msg'] = "data deleted";
        header("location:bin.php");
    }

    public function trash($id = ""){
        $query = "update `mobile` set  `deleted_at` = '".date("Y-m-d")."' where `id` = '".$id."'";
        if($this->link->query($query)){
            $_SESSION['msg'] = "data updated";
            header("location:show.php");
        }


    }

    public function bin(){
        $query = "select * from `mobile` WHERE `deleted_at` is not NULL";
        if($resource = $this->link->query($query)){
            $arr = array();
            while($rcv_resource = mysqli_fetch_assoc($resource)){
                $arr[] = $rcv_resource;
            }
        }
        return $arr;

    }

    public function recover($id = ""){
        $query = "update `mobile` set  `deleted_at` = null where `id` = '".$id."'";
        if($this->link->query($query)){
            $_SESSION['msg'] = "data updated";
            header("location:bin.php");
        }


    }


}